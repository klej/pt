angular.module('ptApp', ['ui.router'])
    .config(function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/list');
        $stateProvider
            .state('contacts', {
                abstract: true,
                views: {
                    'header': {
                        templateUrl: 'templates/header.html'
                    },
                    'container': {
                        template: '<ui-view></ui-view>'
                    }
                }
            })
            .state('contacts.array', {
                abstract: true,
                templateUrl: 'templates/bar.html'
            })
            .state('contacts.array.list', {
                url: '/list',
                templateUrl: 'templates/list.html',
                controller: 'ArrayController',
                controllerAs: 'vm'
            })
            .state('contacts.array.tiles', {
                url: '/tiles',
                templateUrl: 'templates/tiles.html',
                controller: 'ArrayController',
                controllerAs: 'vm'
            })
            .state('contacts.details', {
                url: '/details/:id',
                templateUrl: 'templates/details.html',
                controller: 'DetailsController',
                controllerAs: 'vm'
            })
            .state('contacts.new', {
                url: '/new',
                templateUrl: 'templates/new.html',
                controller: 'NewContactController',
                controllerAs: 'vm'
            })
            .state('contacts.edit', {
                url: '/edit/:id',
                templateUrl: 'templates/edit.html',
                controller: 'DetailsController',
                controllerAs: 'vm'
            })
            .state('contacts.addlink', {
                url: '/link/:id',
                templateUrl: 'templates/link.html',
                controller: 'LinksController',
                controllerAs: 'vm'
            });
    })
    .run(function(ContactsService) {
        var fileName = '../ab.xml';
        ContactsService.parseFile(fileName);
    })

    .controller('ArrayController', arrayController)
    .controller('DetailsController', detailsController)
    .controller('NewContactController', newContactsController)
    .controller('LinksController', linksController)


    .factory('ContactsService', contactsService)
    .factory('LinkService', linkService);


function arrayController(ContactsService) {
    var vm = this;

    vm.contacts = ContactsService.getContacts();

    vm.remove = function(contactID) {
        ContactsService.removeContact(contactID);
    };
}

function detailsController($state, $stateParams, ContactsService, LinkService) {
    var vm = this,
        contactID = $stateParams.id;

    vm.contact = ContactsService.selectContact(contactID);
    if(!vm.contact) {
        $state.go('contacts.array.list')
    }

    vm.remove = function() {
        ContactsService.removeContact(contactID);
        $state.go('contacts.array.list');
    };

    vm.reset = function() {
        vm.contact = ContactsService.resetContact();
    };

    vm.editContact = function() {
        ContactsService.editContact(vm.contact);
        $state.go('contacts.details', {id: contactID});
    };

    vm.unlink = function(linkID) {
        LinkService.unlink(vm.contact, linkID);
        ContactsService.editContact(vm.contact);
    };

    vm.isLinksExist = function() {
        return !(angular.equals({}, vm.contact.Links) ||
        typeof vm.contact.Links === "undefined");
    };
}

function newContactsController($state, $scope, ContactsService) {
    var vm = this;
    vm.newContact = {};

    vm.addContact = function() {
        var contactID = ContactsService.addContact(vm.newContact);
        $state.go('contacts.details', {id: contactID});
    };

    vm.clearForm = function() {
        vm.newContact = {};
        $scope.newForm.$setPristine();
    };
}

function linksController($state, $stateParams, ContactsService, LinkService) {
    var vm = this,
        contactID = $stateParams.id;

    vm.contact = ContactsService.selectContact(contactID);
    if(!vm.contact) {
        $state.go('contacts.array.list')
    }
    else {
        vm.contact.Links = vm.contact.Links || {};
        vm.linkContacts = LinkService.getContacts(vm.contact);
    }

    vm.select = function(linkContact) {
        var linkID = linkContact.CustomerID;

        if(vm.contact.Links.hasOwnProperty(linkID)) {
            delete vm.contact.Links[linkID];
        }
        else {
            vm.contact.Links[linkID] = linkContact;
        }
    };

    vm.save = function() {
        ContactsService.editContact(vm.contact);
        $state.go('contacts.details', {id: vm.contact.CustomerID});
    };
}


function contactsService($http) {
    var contacts,
        contact;

    var service = {
        parseFile: parseFile,
        getContacts: getContacts,
        selectContact: selectContact,
        addContact: addContact,
        editContact: editContact,
        resetContact: resetContact,
        removeContact: removeContact
    };
    return service;

    function parseFile(fileName) {
        return $http.get(
            fileName,
            {
                transformResponse: function(data) {
                    var x2js = new X2JS();
                    var json = x2js.xml_str2json(data);
                    return json;
                }
            }
        ).then(function(response) {
                contacts = response.data.AddressBook.Contact;
            });
    }

    function getContacts() {
        return contacts;
    }

    function selectContact(contactID) {
        contact = null;
        contacts.forEach(function(item) {
            if(item.CustomerID === contactID) {
                contact = item;
            }
        });
        if(contact.Links) {
            checkLinks();
        }
        return angular.copy(contact);
    }

    function editContact(newContact) {
        angular.extend(contact, newContact);
    }

    function resetContact() {
        return angular.copy(contact);
    }

    function addContact(newContact) {
        var contactID = Date.now();
        newContact.CustomerID = contactID.toString();
        contacts.push(newContact);
        return contactID;
    }

    function removeContact(contactID) {
        contacts.forEach(function(item, index) {
            if(item.CustomerID === contactID) {
                contacts.splice(index, 1);
            }
        });
    }

    function checkLinks() {
        var linksCopy = angular.copy(contact.Links);

        contacts.forEach(function(item) {
            delete linksCopy[item.CustomerID];
        });
        for(var linkID in linksCopy) {
            delete contact.Links[linkID];
        }
    }
}

function linkService(ContactsService) {
    var service = {
        getContacts: getContacts,
        unlink: unlink
    };
    return service;

    function getContacts(contact) {
        var contacts = ContactsService.getContacts();
        var arr = contacts.filter(function(elem) {
            return elem.CustomerID !== contact.CustomerID;
        });
        return arr;
    }

    function unlink(contact, linkID) {
        delete contact.Links[linkID];
    }
}
